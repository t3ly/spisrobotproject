# Mustafa Gobulukoglu A12045952
# Tyler Ly A11962307
# Triple Sonar Completed V.3
# Smoother movement but less reliable collision avoidance
import RPi.GPIO as GPIO, sys, threading, time
import math
GPIO.setmode(GPIO.BOARD)
GPIO.setup(12, GPIO.IN)
GPIO.setup(13, GPIO.IN)
GPIO.setup(19, GPIO.OUT)
RF=GPIO.PWM(19, 20)
RF.start(0)
GPIO.setup(21, GPIO.OUT)
RB=GPIO.PWM(21, 20)
RB.start(0)
GPIO.setup(24, GPIO.OUT)
LF=GPIO.PWM(24,20)
LF.start(0)
GPIO.setup(26, GPIO.OUT)
LB=GPIO.PWM(26,20)
LB.start(0)

slowspeed = 10
mediumspeed = 25
fastspeed = 50

def forwards():
  RF.ChangeDutyCycle(fastspeed+6)
  RB.ChangeDutyCycle(0)
  LF.ChangeDutyCycle(fastspeed)
  LB.ChangeDutyCycle(0)
  #setLEDs(1, 0, 0, 1)

def reverse():
  RF.ChangeDutyCycle(0)
  RB.ChangeDutyCycle(mediumspeed)
  LF.ChangeDutyCycle(0)
  LB.ChangeDutyCycle(mediumspeed)
  #setLEDs(0, 1, 1, 0)

def turnleft():
  RF.ChangeDutyCycle(fastspeed)
  RB.ChangeDutyCycle(0)
  LF.ChangeDutyCycle(slowspeed)
  LB.ChangeDutyCycle(0)
  #setLEDs(0, 0, 1, 1)

def turnright():
  RF.ChangeDutyCycle(slowspeed)
  RB.ChangeDutyCycle(0)
  LF.ChangeDutyCycle(fastspeed)
  LB.ChangeDutyCycle(0)

def stopall():
  RF.ChangeDutyCycle(0)
  RB.ChangeDutyCycle(0)
  LF.ChangeDutyCycle(0)
  LB.ChangeDutyCycle(0)

def reverseLeft():
  RF.ChangeDutyCycle(0)
  RB.ChangeDutyCycle(slowspeed)
  LF.ChangeDutyCycle(0)
  LB.ChangeDutyCycle(fastspeed)

def perfectTurnLeft():
  RF.ChangeDutyCycle(mediumspeed)
  RB.ChangeDutyCycle(0)
  LF.ChangeDutyCycle(0)
  LB.ChangeDutyCycle(mediumspeed)

def perfectTurnRight():
  RF.ChangeDutyCycle(0)
  RB.ChangeDutyCycle(mediumspeed)
  LF.ChangeDutyCycle(mediumspeed)
  LB.ChangeDutyCycle(0)

def sonarVal(setin, setout, L):
  GPIO.setup(setout, GPIO.OUT)
  GPIO.output(setout, True)
  time.sleep(0.00001)
  GPIO.output(setout, False)
  start = time.time()
  count = time.time()
  GPIO.setup(setin,GPIO.IN)
  while GPIO.input(setin)==0 and time.time() - count<0.1:
	start = time.time()
  stop =time.time()
  while GPIO.input(setin)==1:
  	stop = time.time()
  elapsed = stop -start
  distance = (elapsed*34000)/2
  return  L + [distance]

SONAR = 8
LEFTTRIG = 22
LEFTECHO = 18
RIGHTTRIG = 13
RIGHTECHO = 12

try:
  distanceListM = []
  distanceListR = []
  distanceListL = []
  while True:
       distanceListM =sonarVal(SONAR,SONAR,distanceListM)
       distanceListL =sonarVal(LEFTECHO,LEFTTRIG,distanceListL)
       distanceListR =sonarVal(RIGHTECHO,RIGHTTRIG,distanceListR)
       if len(distanceListM) >= 3 and len(distanceListL)>=3 and len(distanceListR) >=3:
         NM = distanceListM
         NL = distanceListL
         NR = distanceListR
	 NM.sort()
	 NL.sort()
	 NR.sort()
         medianM = len(distanceListM)/2
         medianL = len(distanceListL)/2
         medianR = len(distanceListR)/2
         deterM = NM[medianM] #distanceListM[medianM]# + distanceList
         deterL = NL[medianL] #distanceListL[medianL]# + distanceListL[medianL-1] + distanceListL[medianL+1])/3
         deterR = NR[medianR] #distanceListR[medianR]# + distanceListR[medianR-1] + distanceListR[medianR+1])/3
	 if deterL<=10 and deterR <=10:
           stopall()
 	   time.sleep(0.1)
	   reverse()
	   time.sleep(.5)
	   perfectTurnRight()
	   time.sleep(.45)
	 elif deterL <=5 and deterM <=10:
	   reverse()
	   time.sleep(.25)
         elif deterL <=15 and deterM <=15:
	   #reverse()
	   #time.sleep(.1)
	   perfectTurnRight()
           time.sleep(.3)
	   turnright()
           time.sleep(.1)
 	 elif deterL <=20 and deterM >20:
	   turnright()
	   time.sleep(.1)
	 elif deterR <=5 and deterM <=10:
	   reverse()
	   time.sleep(.25)
         elif deterR <=15 and deterM <=15:
	   #reverse()
           #time.sleep(.1)
	   perfectTurnLeft()
	   time.sleep(.3)
	   turnleft()
	   time.sleep(.1)
	 elif deterR <= 20 and deterM >20:
	   turnleft()
	   time.sleep(.1)
	 elif deterM<=4 or deterL <=4 or deterR<=4:
	   stopall()
	   time.sleep(1)
	   reverse()
	   time.sleep(.6)
	   perfectTurnLeft()
	   time.sleep(.45)
         else:
           forwards()
	 distanceListM = []#distanceListM[1:]
	 distanceListL = []#distanceListL[1:]
 	 distanceListR = [] #distanceListR[1:]

except KeyboardInterrupt:
  Going = False
  GPIO.cleanup()
  sys.exit()
