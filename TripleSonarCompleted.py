# Mustafa Gobulukoglu A12045952
# Tyler Ly A11962307
# Triple Sonar Completed V.1
import RPi.GPIO as GPIO, sys, threading, time
import math
GPIO.setmode(GPIO.BOARD)
GPIO.setup(12, GPIO.IN)
GPIO.setup(13, GPIO.IN)
GPIO.setup(19, GPIO.OUT)
RF=GPIO.PWM(19, 20)
RF.start(0)
GPIO.setup(21, GPIO.OUT)
RB=GPIO.PWM(21, 20)
RB.start(0)
GPIO.setup(24, GPIO.OUT)
LF=GPIO.PWM(24,20)
LF.start(0)
GPIO.setup(26, GPIO.OUT)
LB=GPIO.PWM(26,20)
LB.start(0)

slowspeed = 10
mediumspeed = 25
fastspeed = 50
LED1 = 22
LED2 = 18
LED3 = 11
LED4 = 07
GPIO.setup(LED1, GPIO.OUT)
GPIO.setup(LED2, GPIO.OUT)
GPIO.setup(LED3, GPIO.OUT)
GPIO.setup(LED4, GPIO.OUT)

def forwards():
  RF.ChangeDutyCycle(fastspeed+7)
  RB.ChangeDutyCycle(0)
  LF.ChangeDutyCycle(fastspeed)
  LB.ChangeDutyCycle(0)
  #setLEDs(1, 0, 0, 1)

def reverse():
  RF.ChangeDutyCycle(0)
  RB.ChangeDutyCycle(mediumspeed)
  LF.ChangeDutyCycle(0)
  LB.ChangeDutyCycle(mediumspeed)
  #setLEDs(0, 1, 1, 0)

def turnleft():
  RF.ChangeDutyCycle(fastspeed)
  RB.ChangeDutyCycle(0)
  LF.ChangeDutyCycle(slowspeed)
  LB.ChangeDutyCycle(0)
  #setLEDs(0, 0, 1, 1)

def turnright():
  RF.ChangeDutyCycle(slowspeed)
  RB.ChangeDutyCycle(0)
  LF.ChangeDutyCycle(fastspeed)
  LB.ChangeDutyCycle(0)
  #setLEDs(1, 1, 0, 0)

def stopall():
  RF.ChangeDutyCycle(0)
  RB.ChangeDutyCycle(0)
  LF.ChangeDutyCycle(0)
  LB.ChangeDutyCycle(0)
  #setLEDs(1, 1, 1, 1)

def reverseLeft():
  RF.ChangeDutyCycle(0)
  RB.ChangeDutyCycle(slowspeed)
  LF.ChangeDutyCycle(0)
  LB.ChangeDutyCycle(fastspeed)

def perfectTurnLeft():
  RF.ChangeDutyCycle(mediumspeed)
  RB.ChangeDutyCycle(0)
  LF.ChangeDutyCycle(0)
  LB.ChangeDutyCycle(mediumspeed)

def perfectTurnRight():
  RF.ChangeDutyCycle(0)
  RB.ChangeDutyCycle(mediumspeed)
  LF.ChangeDutyCycle(mediumspeed)
  LB.ChangeDutyCycle(0)

 # switch all LEDs off

SONAR = 8
LEFTTRIG = 22
LEFTECHO = 18
RIGHTTRIG = 13
RIGHTECHO = 12

try:
  distanceListM = []
  distanceListR = []
  distanceListL = []
  while True:
       GPIO.setup(SONAR, GPIO.OUT)
       GPIO.output(SONAR, True)
       time.sleep(0.00001)
       GPIO.output(SONAR, False)
       start = time.time()
       count = time.time()
       GPIO.setup(SONAR, GPIO.IN)
       while GPIO.input(SONAR)==0 and time.time()-count<0.1:
           start = time.time()
       stop=time.time()
       while GPIO.input(SONAR)==1:
           stop = time.time()
       # Calculate pulse length
       elapsed = stop-start
       # Distance pulse travelled in that time is time
       # multiplied by the speed of sound (cm/s)
       distanceM = elapsed * 34000
       # That was the distance there and back so halve the time
       distanceM = distanceM / 2
       distanceListM  = distanceListM  + [distanceM]
       GPIO.setup(LEFTTRIG, GPIO.OUT)
       GPIO.output(LEFTTRIG, True)
       time.sleep(0.00001)
       GPIO.output(LEFTTRIG, False)
       start = time.time()
       count = time.time()
       GPIO.setup(LEFTECHO, GPIO.IN)
       while GPIO.input(LEFTECHO)==0 and time.time()-count<0.1:
           start = time.time()
       stop = time.time()
       while GPIO.input(LEFTECHO)==1:
           stop = time.time()
       elapsed = stop-start
       distanceL = (elapsed*34000)/2
       distanceListL = distanceListL + [distanceL]
       GPIO.setup(RIGHTTRIG, GPIO.OUT)
       GPIO.output(RIGHTTRIG, True)
       time.sleep(0.00001)
       GPIO.output(RIGHTTRIG, False)
       start = time.time()
       stop = time.time()
       GPIO.setup(RIGHTECHO, GPIO.IN)
       while GPIO.input(RIGHTECHO)==0 and time.time()-count<0.1:
           start = time.time()
       stop = time.time()
       while GPIO.input(RIGHTECHO)==1:
           stop = time.time()
       elapsed = stop - start
       distanceR = (elapsed*34000)/2
       distanceListR = distanceListR + [distanceR]
       if len(distanceListM) >= 3 and len(distanceListL)>=3 and len(distanceListR) >=3:
         NM = distanceListM
         NL = distanceListL
         NR = distanceListR
	 NM.sort()
	 NL.sort()
	 NR.sort()
         medianM = len(distanceListM)/2
         medianL = len(distanceListL)/2
         medianR = len(distanceListR)/2
         deterM = NM[medianM] #distanceListM[medianM]# + distanceList
         deterL = NL[medianL] #distanceListL[medianL]# + distanceListL[medianL-1] + distanceListL[medianL+1])/3
         deterR = NR[medianR] #distanceListR[medianR]# + distanceListR[medianR-1] + distanceListR[medianR+1])/3
         if deterM <= 10 or deterR <=10:
           stopall()
	   time.sleep(.1)
	   reverse()
	   time.sleep(.15)
	   perfectTurnLeft()
	   time.sleep(.35)
	   print "Middle sensor distance: " + str(deterM)
           print "Right sensor distance: " + str(deterR)
         elif deterL <=10:
           stopall()
           time.sleep(.1)
           reverse()
           time.sleep(.15)
           perfectTurnRight()
           time.sleep(.35)
           print "Left sensor distance: " + str(deterL)
         else:
           forwards()
	 distanceListM = []#distanceListM[1:]
	 distanceListL = []#distanceListL[1:]
 	 distanceListR = []# distanceListR[1:]
       

except KeyboardInterrupt:
  Going = False
  GPIO.cleanup()
  sys.exit()
